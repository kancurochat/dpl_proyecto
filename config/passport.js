const passport = require('passport');

const LocalStrategy = require('passport-local').Strategy;

const Cliente = require('../models/Cliente');

passport.use(new LocalStrategy(

    function(email, password, done) {

        Cliente.findOne({email:email}, function(err, cliente) {

            if (err) return done(err);

            if(!cliente) return done(null, false, {message: "Email inexistente o incorrecto"});

            if(!cliente.validPassword(password)) return done(null, false, {message: "Password incorrecto"});

            return done(null, cliente);

        });

    }

));

passport.serializeUser(function(user, cb) {

    cb(null, user.id);

});

passport.deserializeUser(function(id, cb) {

    Cliente.findById(id, function(err, cliente) {

        cb(err, cliente);

    });

});

module.exports = passport;