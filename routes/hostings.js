var express = require('express');
var router = express.Router();

let hostingController = require('../controllers/hosting');

router.get('/', hostingController.get_hostings);

module.exports = router;