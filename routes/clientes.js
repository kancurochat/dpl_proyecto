var express = require('express');
var router = express.Router();

let clienteController = require('../controllers/cliente');

// Recibe la tabla de los clientes
router.get('/', clienteController.get_clientes);

// Recibe el formulario de creación 
router.get('/create', clienteController.get_create_clientes);
// Manda los datos del formulario
router.post('/create', clienteController.post_create_clientes);

// Recibe el formulario de actualiación
router.get('/:id/update', clienteController.get_update_clientes);
// Manda los datos del formulario
router.post('/:id/update', clienteController.post_update_clientes);

// Elimina un cliente
router.post('/:id/delete', clienteController.delete);

module.exports = router;
