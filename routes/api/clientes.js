let express = require('express');

let router = express.Router();

let clientesControllerAPI = require('../../controllers/api/clientesControllerAPI');

router.get('/', clientesControllerAPI.getClientes);

router.post('/create', clientesControllerAPI.create_cliente);

router.post('/hosting', clientesControllerAPI.get_hosted);

router.delete('/delete', clientesControllerAPI.delete_cliente);

router.put('/update', clientesControllerAPI.update_cliente);

module.exports = router;