let express = require('express');

let router = express.Router();

let serverControllerAPI = require('../../controllers/api/serverControllerAPI');

router.get('/', serverControllerAPI.server_list);

router.post('/create', serverControllerAPI.server_create);

router.delete('/delete', serverControllerAPI.server_delete);

router.put('/update', serverControllerAPI.server_update);

module.exports = router;