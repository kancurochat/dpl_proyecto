let express = require('express');

let router = express.Router();

let hostingControllerAPI = require('../../controllers/api/hostingControllerAPI');

router.get('/', hostingControllerAPI.getHostings);

router.delete('/delete', hostingControllerAPI.hosting_delete);

module.exports = router;