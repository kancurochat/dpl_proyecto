var authControllerAPI = require('../../controllers/api/authControllerAPI');
var express = require('express');

var router = express.Router();

router.post('/authenticate', authControllerAPI.authenticate);

module.exports = router;