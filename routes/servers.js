let express = require('express');

let router = express.Router();

let serverController = require('../controllers/server');

// Tabla de servidores
router.get('/', serverController.server_list);

// Recibe el formulario de creación de servidor
router.get('/create', serverController.get_create_server);
// Manda los datos por POST 
router.post('/create', serverController.post_create_server);

// Recibe el formulario de actualización de servidores
router.get('/:id/update', serverController.get_update_server);
// Actualiza los datos por POST
router.post('/:id/update', serverController.post_update_server);

// Recibe el id documento y lo borra
router.post('/:id/delete', serverController.delete_server);

module.exports = router;