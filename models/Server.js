// Importo mongoose
let mongoose = require('mongoose');

// Instancio la clase Schema de mongoose
let Schema = mongoose.Schema;

// Creo el esquema de mis servidores
let serverSchema = new Schema({

    serverId: Number,

    almacenamiento: String,

    ram: String,

    ubicacion: String

});

// Creo un constructor
let Server = function(id, almacenamiento, ram, ubicacion) {

    this.id = id;

    this.almacenamiento = almacenamiento;
    
    this.ram = ram;

    this.ubicacion = ubicacion;

}

// Obtiene un documento con todos los servidores
serverSchema.statics.allServers = function (cb) {

    return this.find({}, cb);

}

// Añade un servidor a la colección de servidores
serverSchema.statics.add = function (server, cb) {

    return this.create(server, cb);

}

// Encuentra un servidor en la colección mediante el id
serverSchema.statics.findById = function (id, cb) {

    return this.findOne(id, cb);

}

// Borra un servidor mediante el _id
serverSchema.statics.removeById = function (id, cb) {

    return this.findByIdAndDelete(id, cb);

}

serverSchema.statics.update = function (id, newServer, cb) {

    return this.findByIdAndUpdate(id, newServer, cb);

}

module.exports = mongoose.model("Server", serverSchema);