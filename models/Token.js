// Importo modulos
let mongoose = require('mongoose');

// Importo Schema
let Schema = mongoose.Schema;

let tokenSchema = new Schema ({

    _userId: {

        type: mongoose.Schema.Types.ObjectId,

        required: true,

        ref: "Cliente"

    },

    token: {

        type: String,

        required: true

    },

    creatAt: {

        type: Date, 

        required: true,

        default: Date.now,

        expires: 43200

    }

});

module.exports = mongoose.model("Token", tokenSchema);