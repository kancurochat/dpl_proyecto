// Importo el modulo moment para el manejo de fechas
const moment = require('moment');

// Importo mongoose
let mongoose = require('mongoose');

// El Schema de mongoose
let Schema = mongoose.Schema;

// El esquema de mi modelo Hosting
let hostingSchema = new Schema ({

    desde: Date,

    hasta: Date,

    servidor: {type: mongoose.Schema.Types.ObjectId, ref: "Server"},

    cliente: {type: mongoose.Schema.Types.ObjectId, ref: "Cliente"}

});

// El constructor del modelo
let Hosting = function (desde, hasta, servidor, cliente) {

    this.desde = desde;

    this.hasta = hasta;

    this.servidor = servidor;

    this.cliente = cliente;

}

// Cuántos días estará hosteada la página del cliente
hostingSchema.methods.diasDeReserva = function() {

    return moment(this.hasta.diff(moment(this.desde), "days") + 1);

}

hostingSchema.statics.hostings = function(cb) {

    return this.find({}, cb);

}

hostingSchema.statics.removeById = function(id, cb) {

    return this.findByIdAndDelete(id, cb);

}

module.exports = mongoose.model("Hosting", hostingSchema);