var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');

const passport = require('./config/passport');
const session = require('express-session');

var Cliente = require('./models/Cliente');
var Token = require('./models/Token');

const store = new session.MemoryStore;

// Conexión a mongodb mediante mongoose
mongoose.connect('mongodb://localhost/proyecto', {useUnifiedTopology: true, useNewUrlParser: true});

mongoose.Promise = global.Promise;

var db = mongoose.connection;

// Vincular la conexión de un evento de error (para obtener 
// notificiaciones de errores en la conexión a la BD)
db.on("error", console.error.bind('Error de conexión con MongoDB'));

// Routers de vistas
var indexRouter = require('./routes/index');
var clienteRouter = require('./routes/clientes');
var serverRouter = require('./routes/servers');
var hostingRouter = require('./routes/hostings');
var tokenRouter = require('./routes/tokens');
//var hostingRouter = require('./routes/hostings');

// Routers de API
var serverAPIRouter = require('./routes/api/servers');
var clienteAPIRouter = require('./routes/api/clientes');
var hostingAPIRouter = require('./routes/api/hostings');
var authAPIRouter = require('./routes/api/auth');

var app = express();

app.use(session({

  cookie: {magAge: 240*60*60*1000}, //Tiempo en milisegundos

  store: store,

  saveUninitialized: true,


  resave: "true",


  secret: "cualquier cosa no pasa nada 477447"

}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Secret key
app.set('secretKey', 'JWT_PDW_!!223344');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Usamos passport
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

// Uso de rutas de vistas
app.use('/', indexRouter);

// ###### RUTAS SESIONES ######

// Vista del login
app.get('/login', function(req, res) {

  res.render("session/login");

});

// Envío del formulario del login
app.post('/login', function(req, res, next) {

  passport.authenticate("local", function (err, cliente, info) {

    if(err) return next(err);

    if (!cliente) return res.render("session/login", {info});

    req.logIn(cliente, function(err) {

      if(err) return next(err);

      return res.redirect('/');

    });

  })(req, res, next);

});

// Redirección del logout
app.get('/logout', function (req, res) {

  req.logOut(); // Limpiamos la sesión

  res.redirect('/');

});

// Vista del formulario de recuperación password
app.get('/forgotPassword', function(req, res) {

  res.render('session/forgotPassword');

});

// Envío del formulario de recuperación password
app.post('/forgotPassword', function(req, res) {

  Cliente.findOne({email: req.body.email}, function(err, cliente) {

    if(!cliente) return res.render('session/forgotPassword', {info: {message: "No existe ese email en nuestra BBDD."}});


    cliente.resetPassword(function(err) {

      if(err) return next(err);

      console.log("session/forgotPasswordMessage");

    });

    res.render('session/forgotPasswordMessage');

  });

});

app.get('/resetPassword/:token', function (req, res, next) {

  Token.findOne({token: req.params.token}, function(err, token) {

    if(!token) return res.status(400).send({type: "not-verified", msg: "No existe un cliente asociado al token. Verifique que su token no haya expirado."});

    Cliente.findById(token._userId, function(err, cliente) {

      if (!cliente) return res.status(400).send({msg: "No existe un cliente asociado al token."});

      res.render("session/resetPassword", {errors: {}, cliente: cliente});

    });

  });

});

app.post('/resetPassword', function(req, res) {

  if(req.body.password != req.body.confirm_password) {

    res.render("session/resetPassword", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, 
    
    cliente: new Cliente({email: req.body.email})});

    return;

  }

  Cliente.findOne({email: req.body.email}, function(err, cliente) {

    cliente.password = req.body.password;

    cliente.save(function(err) {

      if(err) {

        res.render('session/resetPassword', {errors: err.errors, cliente: new Cliente({email: req.body.email})});

      }else {

        res.redirect('/login');

      }

    });

  });

});

function validarUsuario(req, res, next) {

  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {

    if(err) {

      res.json({status:'error', message: err.message, data: null});

    }else {

      req.body.userId = decoded.id;

      console.log('jwt verify: ' + decoded);

      next();

    }

  });

}

app.use('/servers', loggedIn, serverRouter);
app.use('/clientes', clienteRouter);
app.use('/hostings', hostingRouter);
app.use('/token', tokenRouter);

//Uso de rutas de API
app.use('/api/auth', authAPIRouter);
app.use('/api/servers', validarUsuario, serverAPIRouter);
app.use('/api/clientes', clienteAPIRouter);
app.use('/api/hostings', hostingAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {

  if(req.user) {

    next();

  }else {

    console.log("Usuario no logueado");

    res.redirect('/login');

  }

}

module.exports = app;
