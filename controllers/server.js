let Server = require('../models/Server');

exports.server_list = function(req, res) {

    Server.allServers(function(err, result) {

        if(err) res.status(500).send(err.message);

        res.render("servers/index", {servers: result});

    });

}

exports.get_create_server = function (req,res) {

    res.render('servers/create');

}

exports.post_create_server = function(req, res) {

    let server = new Server({

        serverId: req.body.serverId,

        almacenamiento: req.body.almacenamiento,

        ram: req.body.ram,

        ubicacion: req.body.ubicacion

    });

    Server.add(server, function(err) {

        if(err) res.status(500).send(err.message);

        res.status(201).redirect("/servers");

    });

}

exports.get_update_server = function (req, res) {

    Server.findById({_id: req.params.id}, function(err, result) {

        if(err) res.status(500).send(err.message);

        res.render('servers/update', {server: result});

    });

}

exports.post_update_server = function (req, res) {

    Server.update(req.params.id, req.body, function(err) {

        if(err) res.status(500).send(err.message);

        res.redirect('/servers');

    });

}

exports.delete_server = function (req, res) {

    Server.removeById(req.body.id, function(err) {

        if(err) res.status(500).send(err.message);

        res.redirect('/servers');

    });

}