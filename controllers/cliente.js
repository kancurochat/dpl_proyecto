let Cliente = require('../models/Cliente');

exports.get_clientes = function (req, res) {

    Cliente.allClientes(function(err, clientes) {

        if(err) res.status(500).send(err.message);

        res.render('clientes/index', {clientes: clientes});

    });

}

exports.get_create_clientes = function (req, res) {

    res.render("clientes/create", {errors:{}, cliente: new Cliente()});

}

exports.post_create_clientes = function (req, res) {

    if (req.body.password != req.body.confirm_password) {   // Si las contraseñas no coindicen, se renderiza de nuevo la vista de creación de usuarios
        res.render("clientes/create", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, cliente: new Cliente({nombre: req.body.nombre, email: req.body.email})});
        return;
    }
    Cliente.create({nombre:req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoCliente) { // Si coinciden, se crea un usuario nuevo
        if(err){
            res.render("clientes/create", {errors: err.errors, cliente: new Cliente({nombre: req.body.nombre, email: req.body.email})}); // Si falla la creación, se renderiza otra vez la vista de creación
        } else {
            nuevoCliente.enviar_email_bienvenida(); //Si no, se manda el correo de bienvenida y se redirige a la tabla de usuarios.
            res.redirect("/clientes");
        }
    });

}

exports.get_update_clientes = function(req ,res) {

    Cliente.findById(req.params.id, function (err, cliente) {
        //Llama a la función findById y le pasa como callback la función que renderiza
        // la vista junto con el usuario seleccionado
        res.render('clientes/update', {errors:{}, cliente: cliente})

    });

}

exports.post_update_clientes = function(req, res) {

    let update_values = {nombre: req.body.nombre};

    Cliente.findByIdAndUpdate(req.params.id, update_values, function (err, cliente) {

        if(err){
            console.log(err);
            res.render("clientes/update", {errors: err.errors, cliente: new Cliente({nombre: req.body.nombre, email: req.body.email})});
        }else {
            res.redirect('/clientes');
            return;
        }

    });

}

exports.delete = function (req, res, next) {

    Cliente.findByIdAndDelete(req.body.id, function(err) {

        if(err)
            next(err);
        else
            res.redirect('/clientes');
        
    });

}