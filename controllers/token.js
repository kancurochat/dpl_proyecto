let Cliente = require('../models/Cliente');

let Token = require('../models/Token');

module.exports = {

    confirmationGet: function(req, res, next) {

        Token.findOne({token: req.params.token}, function(err, token) {

            if(!token) return res.status(400).send({type: "not-verified", msg: "No se ha verificado el email"});
             
            Cliente.findById(token._userId, function (err, cliente) {

                if(!cliente) return res.status(400).send({msg: "No encontramos un usuario con ese token"});

                if(cliente.verificado) return res.redirect("/usuarios");

                cliente.verificado = true;

                cliente.save(function(err) {

                    if(err) return res.status(500).send({msg: err.message});

                    res.redirect("/");

                });

            });

        });

    }

}