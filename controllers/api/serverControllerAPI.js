let Server = require('../../models/Server');

exports.server_list = function(req, res) {

    Server.allServers(function(err, servers) {

        if(err) res.status(500).send(err.message);

        res.status(200).json({

            servidores: servers

        });

    });

}

exports.server_create = function (req, res) {

    let server = new Server({

        serverId: req.body.serverId,

        almacenamiento: req.body.almacenamiento,

        ram: req.body.ram,

        ubicacion: req.body.ubicacion

    });

    Server.add(server, function (err, server) {

        if(err) res.status(500).send(err.message);

        res.status(201).json({

            servidor: server

        });

    });

}

exports.server_delete = function (req, res) {

    Server.removeById(req.body._id, function (err, result) {

        if(err) res.status(500).send(err.message);

        res.status(200).json({

            borrado: result

        });

    });

}

exports.server_update = function (req, res) {

    Server.update(req.body._id, req.body, function(err, result) {

        if(err) res.status(500).send(err.message);

        res.status(201).json(req.body);

    });

}