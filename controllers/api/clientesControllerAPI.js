let Cliente = require('../../models/Cliente');

exports.getClientes = function (req, res) {

    Cliente.allClientes(function (err, clientes) {

        if(err) res.status(500).send(err.message);

        res.status(200).json({

            clientes: clientes

        });

    });

}

exports.create_cliente = function (req, res) {

    let cliente = new Cliente({

        nombre: req.body.nombre,

        email: req.body.email,

        password: req.body.password,

    });

    Cliente.add(cliente, function(err, cliente) {

        if(err) res.status(500).send(err.message);

        res.status(201).json({

            cliente: cliente

        });

    });

}

exports.delete_cliente = function (req, res) {

    Cliente.removeById(req.body._id, function(err, result) {

        if(err) res.status(500).send(err.message);

        res.status(200).json({

            borrado: result

        });

    });

}

exports.update_cliente = function (req, res) {

    Cliente.update(req.body._id, req.body, function (err, result) {

        if(err) res.status(500).send(err.message);

        res.status(200).json(req.body);

    });

}


exports.get_hosted = function (req, res) {

    Cliente.findById(req.body._id, function (err, cliente) {

        if(err) res.status(500).send(err.message);

        console.log(cliente);

        cliente.getHosted(req.body.server_id, req.body.desde, req.body.hasta, function(err, result) {

            if(err) res.status(500).send(err.message);

            console.log("Hosteando a " + cliente.nombre + " en el servidor " + req.body.server_id);

            res.status(200).json(result);

        });

    });

}