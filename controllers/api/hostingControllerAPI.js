let Hosting = require('../../models/Hosting');

exports.getHostings = function (req, res) {

    Hosting.hostings(function (err, result) {

        if(err) res.status(500).send(err.message);

        res.status(200).json({

            hostings: result

        });

    });

}

exports.hosting_delete = function (req, res) {

    Hosting.removeById(req.body._id, function(err, result) {

        if(err) res.status(500).send(err.message);

        res.status(200).json({

            borrado: result

        });

    });

}