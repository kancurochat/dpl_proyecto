let Hosting = require('../models/Hosting');

exports.get_hostings = function (req, res) {

    Hosting.hostings(function(err, hostings) {

        if(err) res.status(500).send(err.message);

        res.render('hostings/index', {hostings: hostings});

    });

}